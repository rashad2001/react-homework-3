import React, {Component} from 'react';
import './AlbumList.scss';
import Album from "../Album/Album";
import Modal from "../Modal/Modal";
import Header from "../Header/Header";

class AlbumList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allCards: props.albums,
            modal: false,
            clickedBtnID: null,
            AddedCards: []
        }

    }



    modalShow = (e) => {
        this.setState({
            modal: true,
            clickedBtnID: e.target.id
        });
        console.log(e.target);
        document.querySelector('.App').classList.add('active');
    };



    closeButton = () => {
        this.setState({
            modal: false
        });
        document.querySelector('button').classList.remove('active');
        document.querySelector('.App').classList.remove('active');
    };



    AddToCard = () => {
        let card = this.state.allCards[this.state.clickedBtnID];
        this.state.AddedCards.push(card);
        localStorage.setItem('albums', JSON.stringify(this.state.AddedCards));
        this.closeButton();
    };


    render() {
        const albums = this.state.allCards;

        return <div className='album-list-content'>
            {this.state.modal === false
                ?
                <Header/>
                : null
            }
            <div className="album-list">
                {this.state.modal === false
                    ?

                    albums.map((card) => {
                        return <Album

                            key={card.number}
                            path={card.path}
                            name={card.name}
                            price={card.price}
                            color={card.color}
                            onclick={this.modalShow}/>

                    }) : null
                }
                {this.state.modal ?
                    <Modal closeModalButton={this.closeButton} AddToCard={this.AddToCard}/>
                    : null
                }
            </div>
        </div>
    }
}

export default AlbumList;
