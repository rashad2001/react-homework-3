import React, {Component} from 'react';
import './Album.scss'
import Button from "../Button/Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import { faStar } from '@fortawesome/free-solid-svg-icons'


class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {
            path: props.path,
            name: props.name,
            text: props.text,
            price: props.price,
            color: props.color,
            id: props.id,
            onclick: props.onclick,
            starColor: true
        }

    }

    changeStar = (e) => {
        this.setState({
            starColor: !this.state.starColor
        });

        if (this.state.starColor){
            e.target.style.color = 'gold'
        }else {
            e.target.style.color = 'black'
        }
    };


    render() {
        return (
            <div key={this.state.id} id={this.state.id} className="album">
                <img className='album-img' src={this.state.path} alt="image-1"/>
                <div className='album-info'>
                    <h5 className='album-name'>{this.state.name}</h5>
                    <FontAwesomeIcon className='star-icon' onClick={this.changeStar} icon={faStar}/>
                    <p className='album-text'>{this.state.text}</p>
                    <p className='album-price'>{this.state.price}</p>
                    <Button  id={this.state.id} text='ADD TO CART' onclick={this.state.onclick}/>
                </div>
            </div>
        );
    }
}

export default Card;
