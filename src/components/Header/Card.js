import React, {Component} from 'react';
import './Card.scss'

class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {
            card: props.card
        }
    }

    render() {

        return (
            <div className='header-album' >
                <img className='header-album-img' src={this.state.card.path} alt="image"/>
                <h3>{this.state.card.name}</h3>
            </div>
        );
    }
}

export default Card;
