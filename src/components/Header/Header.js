import React, {Component} from 'react';
import './Header.scss'
import Card from "./Card";


class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            card: false
        };
    }

    showModalWindow = () => {
        this.setState({
            card: !this.state.card
        });
    };

    render() {
        let AddedCards =  JSON.parse(localStorage.getItem('cards'))
        return (
            <div className='header'>

                <a className='header-links' href="Home">HOME</a>
                <a className='header-links' href="About us">ABOUT US</a>
                <a className='header-links' href="Contacts">CONTACTS</a>

                <div className='header-link-card'>
                    <a onClick={this.showModalWindow} className='header-links' href="#">

                    </a>
                    {this.state.card ?
                        <div className='header-card-list'>

                            {AddedCards === null ?
                                <h2>There is nothing here</h2>
                                : AddedCards.map((card, ind) => {
                                    return <Card key={ind} card={card}/>
                                })
                            }

                        </div>
                        : null
                    }
                </div>

            </div>
        );
    }
}

export default Header;
